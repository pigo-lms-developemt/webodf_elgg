WebODF for Elgg 1.8
------------

This is an unofficial port of the ViewerJS, WebODF for elgg. It also comes with support for .docx documents thanks to the OfficeJS Library, Mammoth, and others.

It doesn't use any third party viewer service.