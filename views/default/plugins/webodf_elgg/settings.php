<?php 




$wordc_setting = $vars['entity']->wordc;
$power_setting = $vars['entity']->power;
$pdf_setting = $vars['entity']->pdf;
$msdocx_setting = $vars['entity']->msdocx;

?>

<p>
  <b><?php echo elgg_echo('webodf:settings:msdocx') ?></b>

<?php

echo elgg_view('input/dropdown',array(
'name' => 'params[msdocx]', 
'options_values'=> array( 
                        '0' => '  ', 
                        '1'=> elgg_echo('webodf:settings:options_yes'),
                        '2'=> elgg_echo('webodf:settings:options_no')),
'value'=> $msdocx_setting));

 ?>
</p>


<p>
  <b><?php echo elgg_echo('webodf:settings:odf') ?></b>

<?php

echo elgg_view('input/dropdown',array(
'name' => 'params[wordc]', 
'options_values'=> array( 
                        '0' => '  ', 
                        '1'=> elgg_echo('webodf:settings:options_yes'),
                        '2'=> elgg_echo('webodf:settings:options_no')),
'value'=> $wordc_setting));

 ?>
</p>

<p>
  <b><?php echo elgg_echo('webodf:settings:odp') ?></b>

<?php

echo elgg_view('input/dropdown',array(
'name' => 'params[power]', 
'options_values'=> array( 
                        '0' => '  ', 
                        '1'=> elgg_echo('webodf:settings:options_yes'),
                        '2'=> elgg_echo('webodf:settings:options_no')),
'value'=> $power_setting));

 ?>
</p>

<p>
  <b><?php echo elgg_echo('webodf:settings:pdf') ?></b>

<?php

echo elgg_view('input/dropdown',array(
'name' => 'params[pdf]', 
'options_values'=> array( 
                        '0' => '  ', 
                        '1'=> elgg_echo('webodf:settings:options_yes'),
                        '2'=> elgg_echo('webodf:settings:options_no')),
'value'=> $pdf_setting));

 ?>
</p>