<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$site_url = elgg_get_site_url();
?>

<script src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/jquery/jquery-1.12.4.min.js"></script>
   <script src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/jquery_ui/jquery-ui.min.js"></script>
   
   
 <!--PDF--> 
<link rel="stylesheet" href="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pdf/pdf.viewer.css"> 
<script src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pdf/pdf.js"></script> <!--Docs-->  
 <script src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/docx/jszip-utils.js"></script>
 <script src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/docx/mammoth.browser.min.js"></script>
 
 <link rel="stylesheet" href="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/css/pptxjs.css">
 <link rel="stylesheet" href="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/css/nv.d3.min.css">
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/js/filereader.js"></script>
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/js/d3.min.js"></script>
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/js/nv.d3.min.js"></script>
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/js/pptxjs.js"></script>
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/pptxjs/js/divs2slides.js"></script>
  
  
  <!--All Spreadsheet -->
  <link rel="stylesheet" href="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/SheetJS/handsontable.full.min.css">
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/SheetJS/handsontable.full.min.js"></script>
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/SheetJS/xlsx.full.min.js"></script>
  <!--Image viewer-->
  <link rel="stylesheet" href="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/verySimpleImageViewer/css/jquery.verySimpleImageViewer.css">
  <script type="text/javascript" src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/verySimpleImageViewer/js/jquery.verySimpleImageViewer.js"></script>
  
  
  <script src="<?php echo $site_url ?>mod/webodf_elgg/vendors/officejs/officeToHtml.js"></script>
  
