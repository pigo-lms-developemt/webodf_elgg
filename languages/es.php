<?php
/**
 * 
 */

$spanish = array(

	 
	 
	'webodf:msword:title' => "Documento de Word (Vista Previa)", 
        'webodf:pdf:title' => "Documento PDF (Vista Previa)", 
        'webodf:pptx:title' => "Presentaci&oacute;n de PowerPoint (Vista Previa)", 
    
        'webodf:settings:msdocx' => "¿Habilitar vista previa para documentos de Microsoft Word (.docx)?", 
        'webodf:settings:odf' => "¿Habilitar vista previa para documentos de texto de LibreOffice?", 
        'webodf:settings:odp' => "¿Habilitar vista previa para presentaciones de LibreOffice?", 
        'webodf:settings:pdf' => "¿Habilitar vista previa para archivos PDF?", 
        'webodf:settings:options_yes' => "S&iacute;", 
        'webodf:settings:options_no' => "No",
);

add_translation("es", $spanish);
